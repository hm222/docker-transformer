use candle_transformers::models::bert::{BertModel, Config, DTYPE};

use anyhow::{Error as E, Result};
// use candle::Tensor;
use candle_nn::VarBuilder;
// use hf_hub::{api::sync::Api, Repo, RepoType};
use tokenizers::{PaddingParams, Tokenizer};
use candle_core::{Device, Tensor};
use lambda_runtime::{handler_fn, Context, Error};
use serde_json::{Value, json};
use serde::Deserialize;

#[derive(Deserialize)]
struct BertRequest {
    s1: String,
    s2: String
}

fn build_model_and_tokenizer() -> Result<(BertModel, Tokenizer)> {
    let device = &Device::Cpu;
    // let model_id = "sentence-transformers/all-MiniLM-L6-v2".to_string();
    // let revision = "refs/pr/21".to_string();

    // let repo = Repo::with_revision(model_id, RepoType::Model, revision);
    // let (config_filename, tokenizer_filename, weights_filename) = {
    //     let api = Api::new()?;
    //     let api = api.repo(repo);
    //     let config = api.get("config.json")?;
    //     let tokenizer = api.get("tokenizer.json")?;
    //     let weights = api.get("model.safetensors")?;
    //     (config, tokenizer, weights)
    // };
    
    let config = std::fs::read_to_string("/usr/local/bin/config.json")?;
    let config: Config = serde_json::from_str(&config)?;
    let tokenizer = Tokenizer::from_file("/usr/local/bin/tokenizer.json").map_err(E::msg)?;

    let vb = VarBuilder::from_pth("/usr/local/bin/pytorch_model.bin", DTYPE, device)?;
    let model = BertModel::load(vb, &config)?;
    Ok((model, tokenizer))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(func)).await?;
    Ok(())
}

// pub fn normalize_l2(v: &Tensor) -> Result<Tensor> {
//     Ok(v.broadcast_div(&v.sqr()?.sum_keepdim(1)?.sqrt()?)?)
// }

async fn func(event: Value, _: Context) -> Result<Value, Error> {
    let request: BertRequest = serde_json::from_value(event).map_err(|e| {
        eprintln!("Error parsing event: {:?}", e);
        Error::from(e)
    })?;
    let sentences = [
        request.s1.clone(),
        request.s2.clone()
    ];
    let n_sentences = sentences.len();
    let (model, mut tokenizer) = build_model_and_tokenizer()?;
    // let device = &model.device;
    if let Some(pp) = tokenizer.get_padding_mut() {
        pp.strategy = tokenizers::PaddingStrategy::BatchLongest
    } else {
        let pp = PaddingParams {
            strategy: tokenizers::PaddingStrategy::BatchLongest,
            ..Default::default()
        };
        tokenizer.with_padding(Some(pp));
    }
    let tokens = tokenizer
        .encode_batch(sentences.to_vec(), true)
        .map_err(E::msg)?;
    let token_ids = tokens
        .iter()
        .map(|tokens| {
            let tokens = tokens.get_ids().to_vec();
            Ok(Tensor::new(tokens.as_slice(), &Device::Cpu)?)
        })
        .collect::<Result<Vec<_>>>()?;

    let token_ids = Tensor::stack(&token_ids, 0)?;
    let token_type_ids = token_ids.zeros_like()?;
    println!("running inference on batch {:?}", token_ids.shape());
    let embeddings = model.forward(&token_ids, &token_type_ids)?;
    println!("generated embeddings {:?}", embeddings.shape());
    // Apply some avg-pooling by taking the mean embedding value for all tokens (including padding)
    let (_n_sentence, n_tokens, _hidden_size) = embeddings.dims3()?;
    let embeddings = (embeddings.sum(1)? / (n_tokens as f64))?;
    let embeddings = embeddings;
    println!("pooled embeddings {:?}", embeddings.shape());

    let mut similarities = vec![];
    for i in 0..n_sentences {
        let e_i = embeddings.get(i)?;
        for j in (i + 1)..n_sentences {
            let e_j = embeddings.get(j)?;
            let sum_ij = (&e_i * &e_j)?.sum_all()?.to_scalar::<f32>()?;
            let sum_i2 = (&e_i * &e_i)?.sum_all()?.to_scalar::<f32>()?;
            let sum_j2 = (&e_j * &e_j)?.sum_all()?.to_scalar::<f32>()?;
            let cosine_similarity = sum_ij / (sum_i2 * sum_j2).sqrt();
            similarities.push((cosine_similarity, i, j))
        }
    }
    similarities.sort_by(|u, v| v.0.total_cmp(&u.0));
    let mut most_similar_sentences: Vec<String> = vec![];
    for &(score, i, j) in similarities.iter() {
        most_similar_sentences.push(format!("score: {score:.2} '{}' '{}'", sentences[i], sentences[j]));
    }
    let response = json!({
        "statusCode": 200,
        "headers": { "Content-Type": "application/json" },
        "body": json!(most_similar_sentences).to_string(),
    });

    Ok(response)
}