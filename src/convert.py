import torch
from transformers import AutoModel

def load_from_safetensor(safetensor_path):
    # Implement this function based on how your safetensor files are structured
    # Should return a dict where keys are parameter names and values are tensors
    pass

# Load the model structure from Hugging Face
model = AutoModel.from_pretrained("sentence-transformers/all-MiniLM-L6-v2", torch_dtype=torch.float32)

torch.save(model.state_dict(), 'pytorch_model.bin')
