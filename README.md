# Rust Transformer Lambda Function

This project demonstrates how to use the Rust programming language to deploy a serverless function on AWS Lambda. The function utilizes the Hugging Face model "sentence-transformers/all-MiniLM-L6-v2" to compute the cosine similarity score between two given sentences. This README outlines the process of preparing the model, building the Rust application, containerizing it, and deploying it as an AWS Lambda function.

## Overview

The process involves several key steps:

1. **Model Preparation**: We download the `config.json`, `tokenizer.json`, and generate a `pytorch_model.bin` file Using a Python script (`convert.py`) for the HuggingFace model.

2. **Rust Application**: The Rust application, leveraging the [candle framework](https://github.com/huggingface/candle), loads the Bert model to compute cosine similarity scores between pairs of sentences.

3. **Containerization**: The application is containerized using Docker, with a Dockerfile that prepares the environment, copies necessary files, and sets the application as the entry point.

4. **Deployment**: The Docker image is pushed to AWS ECR and then deployed as an AWS Lambda function.

## Step-by-Step Guide

### Model Preparation

1. Run the `convert.py` Python script to download and prepare the necessary model files (`config.json`, `tokenizer.json`, `pytorch_model.bin`).

### Building the Rust Application

1. Ensure Rust is installed on your system.
2. Navigate to the project directory and build the application using Cargo:

    ```bash
    cargo build --release
    ```

### Containerization and Deployment

1. Build the Docker image using the provided Dockerfile:

    ```bash
    docker build -t rust-transformer:latest .
    ```

2. Push the Docker image to AWS ECR (refer to AWS documentation for steps on creating a repository and pushing an image).

3. Deploy the AWS Lambda function using the image from ECR.

### Invoking the Lambda Function

- The Lambda function expects a JSON payload with the structure `{"s1": "sentence1", "s2": "sentence2"}`.
- Use AWS CLI or SDKs to invoke the function and obtain the cosine similarity score.
- It is also possible to invoke via curl command:
```bash
 curl -X POST "https://8ejiyk4v46.execute-api.us-east-2.amazonaws.com/build" \ 
 -H "Content-Type: application/json" \
-d '{"s1": "The first sentence to compare.", "s2": "The second sentence to compare."}'
```

## Screenshots

- **AWS ECR Image Construction**: ![AWS ECR](aws-ecr.png)
- **Lambda Deployment**: ![Lambda Deployment](lambda.png)
- **Successful Invocation**: ![Successful Invocation](endpoint.png)