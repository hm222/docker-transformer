# Stage 1: Build the application
FROM rust:latest as builder
RUN USER=root cargo new --bin rust-transformer
WORKDIR /rust-transformer

# Copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

COPY ./src ./src

RUN touch src/*.rs && cargo build --release

FROM debian:stable
RUN apt-get update && apt-get install -y libssl-dev && rm -rf /var/lib/apt/lists/*
COPY --from=builder /rust-transformer/src/pytorch_model.bin /usr/local/bin/pytorch_model.bin
COPY --from=builder /rust-transformer/src/config.json /usr/local/bin/config.json
COPY --from=builder /rust-transformer/src/tokenizer.json /usr/local/bin/tokenizer.json
COPY --from=builder /rust-transformer/target/release/rust-transformer /usr/local/bin/rust-transformer

# Command to run
ENTRYPOINT ["/usr/local/bin/rust-transformer"]
